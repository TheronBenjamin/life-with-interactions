# Nom du projet : Life With Interactions

## Description du projet

Le projet à pour objectif de réaliser un site en 3D au sein d'un navigateur web.

Il a pour but d'associer les métiers de développeur et de designer afin de rendre une expérience utilisateur interactive.

C'est un projet qui a pour motivation d'ajouter un trait artistique aux sites actuels.

### Elements Principaux

- Réalisation des modèles 3D via Blender
- Intégration de ces modèles 3D sur le framework React avec des librairies 3D
- Interaction avec la souris de l'utilisateur pour créer du mouvement au scroll
- Recherches : Exemple de sites référents :
    * https://halfof8.com/
    * https://my.akinod.fr/fr/A01/black/pc/2/Martin/montagneete
    * https://www.itsoffbrand.com/

# Choix des Technologies :
- React
- Scss
- HTML
- Blender (modélisation 3D)
- Librairies :
    * GSAP
    * Three.js
    * Three.fiber
    * ...

# Planning :

### 07/11/2023

- Création de l'application

```
npx create-react-app life-with-interactions
```

### Novembre Jusqu’à décembre
- formation sur blender et react + librairies three.fiber / three.js / gsap

### Janvier / février

- création de mes modèles 3D + Design du site

### Mars / Avril
- intégration des modèles dans le code + (CICD)

### Juin / Juillet
- Finalisation et mise en production du site

# Fonctionnement du projet

- Installation du projet :

### `npm install`

- Run le projet :

### `npm start`
Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

- Installation d'une dépendance exemple :

### `npm install three @react-three/fiber`


## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can't go back!**

If you aren't satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you're on your own.

You don't have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn't feel obligated to use this feature. However we understand that this tool wouldn't be useful if you couldn't customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: [https://facebook.github.io/create-react-app/docs/code-splitting](https://facebook.github.io/create-react-app/docs/code-splitting)

### Analyzing the Bundle Size

This section has moved here: [https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size](https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size)

### Making a Progressive Web App

This section has moved here: [https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app](https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app)

### Advanced Configuration

This section has moved here: [https://facebook.github.io/create-react-app/docs/advanced-configuration](https://facebook.github.io/create-react-app/docs/advanced-configuration)

### Deployment

This section has moved here: [https://facebook.github.io/create-react-app/docs/deployment](https://facebook.github.io/create-react-app/docs/deployment)

### `npm run build` fails to minify

This section has moved here: [https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify](https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify)
